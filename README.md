# The Software Guild #
## Anna Yesberger ##
### 2016/2017 ###

## Album Page Progress ##
* Album Page V1 (html) * Source
* [Album Page V2](http://ayesberger.bitbucket.org/albumpagev2/index.html) (html + CSS) * [Source](https://bitbucket.org/ayesberger/ayesberger.bitbucket.org/src/3fbaedd7efb7917eb91926d325386b70da3b5626/albumpagev2/?at=master)
* [Album Page V3](http://ayesberger.bitbucket.org/albumpagev3/index.html) (html + CSS) * [Source](https://bitbucket.org/ayesberger/ayesberger.bitbucket.org/src/17a5b88babf7efeab4c8f1c5d27b4fb733d0a645/albumpagev3/?at=master)

## Restaurant Site Progress ##
* [Restaurant Site V1 ](http://ayesberger.bitbucket.org/restaurantsitev1/index.html) (html) * [Source](https://bitbucket.org/ayesberger/ayesberger.bitbucket.org/src/3f0489684dd92d2790e75beddc634fa5446ed76d/restaurantsitev1/?at=master)
* [Restaurant Site V2](http://ayesberger.bitbucket.org/restaurantsitev2/index.html) (html and CSS) * [Source](https://bitbucket.org/ayesberger/ayesberger.bitbucket.org/src/3f0489684dd92d2790e75beddc634fa5446ed76d/restaurantsitev2/?at=master)
* [Restaurant Site V3](http://ayesberger.bitbucket.org/restaurantsitev3/index.html) (html + CSS) * [Source](https://bitbucket.org/ayesberger/ayesberger.bitbucket.org/src/17a5b88babf7efeab4c8f1c5d27b4fb733d0a645/restaurantsitev3/?at=master)